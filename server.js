const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const PORT = 3000;

const app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors())

const ALBUM_HARDCODE = {
  title: 'Diam Nonummy',
  cover: '',
  tracks: [{
    id: 1,
    name: 'Mod Tincidunt Ut Laoreet',
    artist: 'Sam Smith',
    link: '',
    duration: 100
  }, {
    id: 2,
    name: 'Run',
    artist: 'Foo Fighters',
    link: '',
    duration: 200
  }, {
    id: 3,
    name: 'Wolves',
    artist: 'Rise Against',
    link: '',
    duration: 300
  }, {
    id: 4,
    name: 'Hurricane',
    artist: 'Thirty Seconds to Mars',
    link: '',
    duration: 400
  }, {
    id: 5,
    name: 'The Sights',
    artist: 'Enter Shikari',
    link: '',
    duration: 500
  }]
};

/** Routes */
app.get('/album/:id', (req, res) => {
  res.json(ALBUM_HARDCODE);
});

app.listen(PORT);

console.log(`Running on ${PORT} port`);

import angular from 'angular';

import { ApiService } from './api.service';
import { ConfigConstant } from './config.constant';
import { HumanizeDurationFitler } from './humanizeDuration.filter';

export const CommonModule = angular
  .module('app.common', [])
  .service('ApiService', ApiService)
  .constant('config', ConfigConstant)
  .filter('humanizeDuration', HumanizeDurationFitler)
  .name;

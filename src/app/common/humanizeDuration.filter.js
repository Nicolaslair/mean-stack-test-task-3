function padTime(t) {
  return t < 10 ? '0'+t : t;
}

export const HumanizeDurationFitler = function() {
  return function(_seconds) {
    if (typeof _seconds !== 'number' || _seconds === 0 || _seconds < 0) {
      return '00:00';
    }
  
    const hours = Math.floor(_seconds / 3600);
    const minutes = Math.floor((_seconds % 3600) / 60);
    const seconds = Math.floor(_seconds % 60);
  
    const time = [];
  
    if (hours && hours !== 0) {
      time.push(padTime(hours));
    }
  
    time.push(padTime(minutes));
    time.push(padTime(seconds));
  
    return time.join(':');
  };
};

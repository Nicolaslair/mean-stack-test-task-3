export class ApiService {
  constructor(config, $http) {
    this.apiUrl = config.apiUrl;
    this.$http = $http;
  }

  getAlbum(id = 1) {
    return this.$http.get(`${this.apiUrl}/album/${id}`)
      .then((response) => {
        return response.data;
      })
      .catch((err) => {
        console.error(err);
        return null;
      });
  }
}

export class AlbumService {
  constructor(ApiService) {
    this.ApiService = ApiService;

    this.selectedTrackId = null;
    this.activeTrackId = null;
    this.album = null;

    this.albumSubscribers = [];
    this.activeTrackSubscribers = [];
  }

  getAlbum() {
    this.ApiService.getAlbum()
      .then((album) => {
        if (album) {
          this.album = album;
          this.selectedTrackId = album.tracks[0].id;
          this.notifyAlbum();
          this.notifyActiveTrack();
        }
      });
  }
  
  subscribeAlbum(cb) {
    this.albumSubscribers.push(cb);
    return () => {
      this.albumSubscribers = this.albumSubscribers
        .filter((sub) => sub !== cb);
    };
  }

  notifyAlbum() {
    this.albumSubscribers.forEach((cb) => {
      cb(this.album);
    });
  }

  subscribeActiveTrack(cb) {
    this.activeTrackSubscribers.push(cb);
    cb({
      selectedTrackId: this.selectedTrackId,
      activeTrackId: this.activeTrackId
    });
    return () => {
      this.activeTrackSubscribers = this.activeTrackSubscribers
        .filter((sub) => sub !== cb);
    };
  }

  notifyActiveTrack() {
    this.activeTrackSubscribers.forEach((cb) => {
      cb({
        selectedTrackId: this.selectedTrackId,
        activeTrackId: this.activeTrackId
      });
    });
  }

  play(trackId) {
    this.selectedTrackId = trackId;
    this.activeTrackId = trackId;
    this.notifyActiveTrack();
  }

  pause() {
    this.activeTrackId = null;
    this.notifyActiveTrack();
  }
}

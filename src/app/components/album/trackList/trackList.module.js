import angular from 'angular';
import { TrackListComponent } from './trackList.component';
import { TrackItemModule } from './trackItem/trackItem.module';
import './trackList.css';

export const TrackListModule = angular
  .module('track.list', [ TrackItemModule ])
  .component('trackList', TrackListComponent)
  .name;
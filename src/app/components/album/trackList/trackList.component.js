import TrackListTemplate from './trackList.html';

export const TrackListComponent = {
  template: TrackListTemplate,
  controller: TrackListController
};

function TrackListController(AlbumService) {
  this.$onInit = () => {
    this.unsubscribe = AlbumService.subscribeAlbum((album) => {
      if (album) {
        this.tracks = album.tracks;
      }
    });
  };

  this.$onDestroy = () => {
    this.unsubscribe();
  };
}

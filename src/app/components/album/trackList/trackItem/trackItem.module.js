import angular from 'angular';
import { TrackItemComponent } from './trackItem.component';

import './trackItem.css';

export const TrackItemModule = angular
  .module('track.item', [])
  .component('trackItem', TrackItemComponent)
  .name;

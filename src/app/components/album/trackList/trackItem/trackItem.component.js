import TrackItemTemplate from './trackItem.html';

export const TrackItemComponent = {
  template: TrackItemTemplate,
  bindings: {
    item: '<'
  },
  controller: TrackItemController
};

function TrackItemController(AlbumService) {
  this.$onInit = () => {
    this.isActive = false;
    this.unsubscribe = AlbumService.subscribeActiveTrack(({ activeTrackId }) => {
      if (this.item.id === activeTrackId) {
        this.isActive = true;
      }
      else {
        this.isActive = false;
      }
    });
  };

  this.$onDestroy = () => {
    this.unsubscribe();
  };

  this.toggle = () => {
    if (this.isActive) {
      AlbumService.pause();
    }
    else {
      AlbumService.play(this.item.id);
    }
  };
}

import angular from 'angular';

import { AlbumComponent } from './album.component';
import { AlbumService } from './album.service';
import './album.css';
import { ActiveTrackModule } from './activeTrack/activeTrack.module';
import { TrackListModule } from './trackList/trackList.module';

export const AlbumModule = angular
  .module('album', [ ActiveTrackModule, TrackListModule ])
  .component('album', AlbumComponent)
  .service('AlbumService', AlbumService)
  .name;

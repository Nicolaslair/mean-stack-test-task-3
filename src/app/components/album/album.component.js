import AlbumTemplate from './album.html';

export const AlbumComponent = {
  template: AlbumTemplate,
  controller: AlbumController
};

function AlbumController(AlbumService) {
  AlbumService.getAlbum();
}
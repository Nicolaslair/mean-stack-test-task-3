import angular from 'angular';
import { ActiveTrackComponent } from './activeTrack.component';
import './activeTrack.css';
export const ActiveTrackModule = angular
  .module('active.track', [])
  .component('activeTrack', ActiveTrackComponent)
  .name;
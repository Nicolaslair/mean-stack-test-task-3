import ActiveTrackTemplate from './activeTrack.html';

export const ActiveTrackComponent = {
  template: ActiveTrackTemplate,
  controller: ActiveTrackController
};

const TWIST_VALUE = 15;

const sliderOptions = {
  floor: 0,
  ceil: 100,
  showTicks: false,
  showTicksValues: false,
  hidePointerLabels: true,
  hideLimitLabels: true,
  showSelectionBar: true
};

function ActiveTrackController($scope, AlbumService) {
  this.$onInit = function() {
    this.timelineSlider = {
      value: 0,
      options: Object.assign({}, sliderOptions)
    };

    this.volumeSlider = {
      value: 0,
      options: Object.assign({}, sliderOptions)
    };

    this.title = null;
    this.tracks = [];
    this.track = null;
    this.isActive = false;

    this.unsubscribeAlbum = AlbumService.subscribeAlbum((album) => {
      if (album) {
        this.title = album.title;
        this.tracks = album.tracks;
      }
    });
    this.unsubscribeTrack = AlbumService.subscribeActiveTrack(({ selectedTrackId, activeTrackId }) => {
      if (this.track && this.track.id !== selectedTrackId) {
        this.timelineSlider.value = 0;
      }
      this.track = this.tracks
        .find((track) => track.id === selectedTrackId);
      if (activeTrackId) {
        this.isActive = true;
      }
      else {
        this.isActive = false;
      }
      if (this.track) {
        this.timelineSlider.options.ceil = this.track.duration;
      }
    });
  };

  this.$onDestroy = () => {
    this.unsubscribeAlbum();
    this.unsubscribeTrack();
  };

  this.play = (id) => {
    AlbumService.play(id);
  };

  this.pause = () => {
    AlbumService.pause();
  };

  this.restart = () => {
    this.timelineSlider.value = 0;
  };

  this.back = () => {
    const newValue = this.timelineSlider.value - TWIST_VALUE;
    this.timelineSlider.value = newValue < 0 ? 0 : newValue;

  };

  this.front = () => {
    const newValue = this.timelineSlider.value + TWIST_VALUE;
    this.timelineSlider.value = newValue > this.timelineSlider.options.ceil
      ? this.timelineSlider.options.ceil : newValue;

    $scope.$broadcast('rzSliderForceRender'); // library issue
  };
}

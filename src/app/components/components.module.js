import angular from 'angular';

import { AlbumModule } from './album/album.module';

export const ComponentsModule = angular
  .module('app.components', [ AlbumModule ])
  .name;

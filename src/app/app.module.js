import angular from 'angular';

import angularSlider from 'angularjs-slider';
import 'angularjs-slider/dist/rzslider.min.css';

import { AppComponent } from './app.component';
import { ComponentsModule } from './components/components.module';
import { CommonModule } from './common/common.module';
import './app.css';

export const AppModule = angular
  .module('app', [
    ComponentsModule,
    CommonModule,
    angularSlider
  ])
  .component('app', AppComponent);
